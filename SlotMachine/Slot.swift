//
//  Slot.swift
//  SlotMachine
//
//  Created by Neil on 03/11/2014.
//  Copyright (c) 2014 Coupaman. All rights reserved.
//

import Foundation
import UIKit

struct Slot {
    var value = 0
    var image = UIImage(named: "Ace")
    var isRed = true
}